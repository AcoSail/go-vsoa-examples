package main

import (
	"fmt"
	"time"

	"gitea.com/AcoSail/go-vsoa/protocol"
	"gitea.com/AcoSail/go-vsoa/server"
)

// startServer initializes the Go VSOA server.
//
// It creates a new server with the given server options. The server password is set to "123456".
// If no password is needed, the password field can be left empty.
//
// The server registers a one-way handler for the "/datagram" URL. The handler receives a request message.
// It prints the URL and the parameter value.
//
// The server starts serving on the address "localhost:3001" in a separate goroutine.
func startServer() {
	// Initialize the Go VSOA server.
	// Set the server options.
	serverOption := server.Option{
		Password: "123456",
	}

	// Create a new server with the given options.
	s := server.NewServer("golang VSOA datagarm server", serverOption)

	// Define the handler function for the "/datagram" URL.
	h := func(req, res *protocol.Message) {
		// Print the URL and the parameter value.
		fmt.Println("/datagram Handler:", "URL", string(req.URL), "Param:", string(req.Param))
	}

	// Register the "/datagram" URL with the handler function.
	s.AddOneWayHandler("/datagram", h)

	// Start serving on the address "localhost:3001" in a separate goroutine.
	go func() {
		_ = s.Serve("localhost:3001")
	}()
}

func main() {
	startServer()

	for {
		time.Sleep(1 * time.Second)
	}
}
