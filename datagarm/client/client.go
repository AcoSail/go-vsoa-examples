package main

import (
	"encoding/json"
	"fmt"

	"gitea.com/AcoSail/go-vsoa/client"
	"gitea.com/AcoSail/go-vsoa/protocol"
)

// VsoaDatagramCall is a function that sends a datagram call using the VSOA protocol.
//
// It connects to the VSOA server at "localhost:3001" and sends a datagram message
// with the parameter `{"Test Datagarm": "Normal Channel"}`. The function prints an
// error if there was a problem connecting to the server or sending the datagram
// message. Otherwise, it prints "Datagram send done" to indicate a successful send.
func VsoaDatagramCall() {
	// Set client options
	clientOption := client.Option{
		Password: "123456",
	}

	// Create a new client instance
	c := client.NewClient(clientOption)

	// Connect to the VSOA server
	_, err := c.Connect("vsoa", "localhost:3001")
	if err != nil {
		fmt.Println(err)
	}

	// Close the connection when the function exits
	defer c.Close()

	// Create a new message
	req := protocol.NewMessage()

	// Set the message parameter
	req.Param, _ = json.RawMessage(`{"Test Datagarm": "Normal Channel"}`).MarshalJSON()

	// Send the datagram call
	_, err = c.Call("/datagram", protocol.TypeDatagram, protocol.ChannelNormal, req)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Datagram send done")
	}
}

func main() {
	VsoaDatagramCall()
}
