package main

import (
	"encoding/json"
	"errors"
	"fmt"

	"gitea.com/AcoSail/go-vsoa/client"
	"gitea.com/AcoSail/go-vsoa/protocol"
)

type RpcLightParam struct {
	LightStatus bool `json:"Light On"`
}

var lightstatus = false

func VsoaRpcCall() {
	clientOption := client.Option{
		Password: "123456",
	}

	c := client.NewClient(clientOption)
	_, err := c.Connect("vsoa", "localhost:3001")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer c.Close()

	req := protocol.NewMessage()

	// Query the current status of the light.
	reply, err := c.Call("/light", protocol.TypeRPC, protocol.RpcMethodGet, req)
	if err != nil {
		if err == errors.New(protocol.StatusText(protocol.StatusInvalidUrl)) {
			fmt.Println("Pass: Invalid URL")
		} else {
			fmt.Println(err)
		}
	} else {
		DstParam := new(RpcLightParam)
		json.Unmarshal(reply.Param, DstParam)
		lightstatus = DstParam.LightStatus
		fmt.Println("Seq:", reply.SeqNo(), "RPC Get ", "Light On:", DstParam.LightStatus)
	}

	// If the light is currently on, turn it off; if it's off, turn it on.
	if lightstatus {
		req.Param, _ = json.RawMessage(`{"Light On":false}`).MarshalJSON()
	} else {
		req.Param, _ = json.RawMessage(`{"Light On":true}`).MarshalJSON()
	}
	reply, err = c.Call("/light", protocol.TypeRPC, protocol.RpcMethodSet, req)
	if err != nil {
		if err == errors.New(protocol.StatusText(protocol.StatusInvalidUrl)) {
			fmt.Println("Pass: Invalid URL")
		} else {
			fmt.Println(err)
		}
	} else {
		DstParam := new(RpcLightParam)
		json.Unmarshal(reply.Param, DstParam)
		fmt.Println("Seq:", reply.SeqNo(), "RPC Set ", "Light On:", DstParam.LightStatus)
	}

	// Query the status of the light after executing the operation.
	reply, err = c.Call("/light", protocol.TypeRPC, protocol.RpcMethodGet, req)
	if err != nil {
		if err == errors.New(protocol.StatusText(protocol.StatusInvalidUrl)) {
			fmt.Println("Pass: Invalid URL")
		} else {
			fmt.Println(err)
		}
	} else {
		DstParam := new(RpcLightParam)
		json.Unmarshal(reply.Param, DstParam)
		fmt.Println("Seq:", reply.SeqNo(), "RPC Get ", "Light On:", DstParam.LightStatus)
	}
}

func main() {
	VsoaRpcCall()
}
