package main

import (
	"fmt"
	"time"

	"gitea.com/AcoSail/go-vsoa/protocol"
	"gitea.com/AcoSail/go-vsoa/server"
)

// startServer initializes the Go VSOA server.
//
// It creates a new server with the given server options. The server password is set to "123456".
// If no password is needed, the password field can be left empty.
//
// The server registers a one-way handler for the "/datagramQuick" URL. The handler receives a request message.
// It prints the URL and the parameter value.
//
// The server starts serving on the address "localhost:3001" in a separate goroutine.
func startServer() {
	// Initialize the Go VSOA server with the given server options.
	serverOption := server.Option{
		Password: "123456",
	}
	s := server.NewServer("golang VSOA datagram quick server", serverOption)

	// Define the handler for the "/datagramQuick" URL.
	qh := func(req, res *protocol.Message) {
		// Print the URL and parameter value.
		fmt.Println("/datagramQuick Handler:", "URL", string(req.URL), "Param:", string(req.Param))
	}

	// Register the handler for the "/datagramQuick" URL.
	s.AddOneWayHandler("/datagramQuick", qh)

	// Start serving on the address "localhost:3001" in a separate goroutine.
	go func() {
		err := s.Serve("localhost:3001")
		if err != nil {
			fmt.Println("Error starting server:", err)
		}
	}()
}

func main() {
	startServer()

	for {
		time.Sleep(1 * time.Second)
	}
}
