package main

import (
	"encoding/json"
	"fmt"

	"gitea.com/AcoSail/go-vsoa/client"
	"gitea.com/AcoSail/go-vsoa/protocol"
)

// VsoaDatagarmQuickCall is a function that sends a datagram quick call using the VSOA protocol.
//
// It connects to the VSOA server at "localhost:3001" and sends a datagram message
// with the parameter `{"Test Datagarm": "Quick Channel"}`. The function prints an
// error if there was a problem connecting to the server or sending the datagram
// message. Otherwise, it prints "Datagram send done" to indicate a successful send.
func VsoaDatagarmQuickCall() {
	// Set the client options
	clientOption := client.Option{
		Password: "123456",
	}

	// Create a new client
	c := client.NewClient(clientOption)

	// Connect to the VSOA server
	_, err := c.Connect("vsoa", "localhost:3001")
	if err != nil {
		fmt.Println(err)
		return
	}

	// Close the connection when the function exits
	defer c.Close()

	// Create a new message
	req := protocol.NewMessage()

	// Set the parameter of the message
	req.Param, _ = json.RawMessage(`{"Test Datagarm": "Quick Channel"}`).MarshalJSON()

	// Send the datagram quick call
	_, err = c.Call("/datagramQuick", protocol.TypeDatagram, protocol.ChannelQuick, req)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Datagram send done")
	}
}

func main() {
	VsoaDatagarmQuickCall()
}
