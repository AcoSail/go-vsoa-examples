# Examples for go-vsoa

## How to run

clone this repo & install go-vsoa@1.0.2 SDK in your GOPATH.

```sh
cd `go env GOPATH`
git clone https://gitea.com/AcoSail/go-vsoa-examples
cd go-vsoa-examples
go mod tidy
```

If you install succeefullly, you can run examples in this repository.

Enter one sub directory in this repository,  `cd server, go run server.go` in one terminal and `cd client; go run client.go` in another ternimal, and you can watch the run result.

For example,

```sh
cd rpc_sync/server
go run server.go
```

And

```sh
cd rpc_sync/client
go run client.go
```
