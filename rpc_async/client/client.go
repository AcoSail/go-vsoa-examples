package main

import (
	"encoding/json"
	"fmt"

	"gitea.com/AcoSail/go-vsoa/client"
	"gitea.com/AcoSail/go-vsoa/protocol"
)

type RpcLightParam struct {
	LightStatus bool `json:"Light On"`
}

// VsoaRpcAsyncCall is a function that performs asynchronous RPC calls using the Vsoa client.
// It connects to the "vsoa" server at "localhost:3001" and sends two RPC requests ("/light")
// using different message objects. The function waits for the responses of both calls and
// logs the results using the logAsyncCall function.
func VsoaRpcAsyncCall() {
	// Set the client options for connecting to the Vsoa server
	clientOption := client.Option{
		Password: "123456",
	}

	// Create a new Vsoa client with the given options
	c := client.NewClient(clientOption)

	// Connect to the "vsoa" server at "localhost:3001"
	_, err := c.Connect("vsoa", "localhost:3001")
	if err != nil {
		fmt.Println(err)
		return
	}

	// Close the client connection when the function exits
	defer c.Close()

	// Create new message objects for each RPC request
	req1 := protocol.NewMessage()
	req2 := protocol.NewMessage()
	reply := protocol.NewMessage()

	// Send the first RPC request ("/light") asynchronously and get the channel for waiting the response
	Call1 := c.Go("/light", protocol.TypeRPC, protocol.RpcMethodGet, req1, reply, nil).Done

	// Send the second RPC request ("/light") asynchronously and get the channel for waiting the response
	Call2 := c.Go("/light", protocol.TypeRPC, protocol.RpcMethodGet, req2, reply, nil).Done

	// Wait for the responses of both RPC calls and log the results using the logAsyncCall function
	for i := 0; i < 2; i++ {
		select {
		case call := <-Call1:
			logAsyncCall(call)
		case call := <-Call2:
			logAsyncCall(call)
		}
	}
}

func main() {
	VsoaRpcAsyncCall()
}

func logAsyncCall(call *client.Call) {
	reply := call.Reply
	DstParam := new(RpcLightParam)
	json.Unmarshal(reply.Param, DstParam)
	fmt.Println("Seq:", reply.SeqNo(), "RPC Get ", "Light On:", DstParam.LightStatus)
}
