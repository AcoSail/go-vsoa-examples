package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"gitea.com/AcoSail/go-vsoa/client"
	"gitea.com/AcoSail/go-vsoa/protocol"
)

type PublishTestParam struct {
	Publish string `json:"publish"`
}

type callback struct{}

// VsoaGetPublishQuickCall returns a description of the Go function.
//
// This function sets the client options, creates a new client instance,
// connects to the VSOA server, and closes the connection when the function
// exits. It then subscribes to the "/publisher/quick" channel and retrieves
// the publish parameters. After a delay of 2 seconds, it unsubscribes from
// the "/publisher/quick" channel and then subscribes again. It then waits
// for another 2 seconds before returning.
func VsoaGetPublishQuickCall() {
	// Set client options
	clientOption := client.Option{
		Password: "123456",
	}

	// Create a new client instance
	c := client.NewClient(clientOption)

	// Connect to the VSOA server
	_, err := c.Connect("vsoa", "localhost:3001")
	if err != nil {
		fmt.Println(err)
		return
	}

	// Close the connection when the function exits
	defer c.Close()

	cb := new(callback)

	// Subscribe to the "/publisher/quick" channel and retrieve the publish parameters
	// client don't know if it's quick channel or not
	err = c.Subscribe("/publisher/quick", cb.getQuickPublishParam)
	if err != nil {
		// Handle the error
		if err == errors.New(protocol.StatusText(protocol.StatusInvalidUrl)) {
			fmt.Println("Pass: Invalid URL")
			return
		} else {
			fmt.Println(err)
		}
	}

	// Wait for 2 seconds
	time.Sleep(2 * time.Second)

	// Unsubscribe from the "/publisher/quick" channel
	c.UnSubscribe("/publisher/quick")

	// Wait for 2 seconds
	time.Sleep(2 * time.Second)

	// Subscribe again to the "/publisher/quick" channel
	c.Subscribe("/publisher/quick", cb.getQuickPublishParam)

	// Wait for 2 seconds
	time.Sleep(2 * time.Second)
}

func main() {
	VsoaGetPublishQuickCall()
}

// getPublishParam parses the JSON parameter from the message and prints the "Publish" field
func (c callback) getQuickPublishParam(m *protocol.Message) {
	// Create a new PublishTestParam instance
	param := new(PublishTestParam)

	// Unmarshal the JSON parameter into the param variable
	json.Unmarshal(m.Param, param)

	// Print the value of the "Publish" field
	fmt.Println("Param:", param.Publish)
}
