package main

import (
	"encoding/json"
	"time"

	"gitea.com/AcoSail/go-vsoa/protocol"
	"gitea.com/AcoSail/go-vsoa/server"
)

func startServer() {
	// Set the server options.
	serverOption := server.Option{
		Password: "123456",
	}

	// Create a new server with the given options.
	s := server.NewServer("golang VSOA publish server", serverOption)

	// Define the publisher function.
	publishFunc := func(req, _ *protocol.Message) {
		req.Param, _ = json.RawMessage(`{"publish":"GO-VSOA-Publishing-Quick"}`).MarshalJSON()
	}
	// Add the publisher to the server. Publishing is done every 1 second in quick channel.
	s.AddQPublisher("/publisher/quick", 1*time.Second, publishFunc)

	// Start serving on the address "localhost:3001" in a separate goroutine.
	go func() {
		_ = s.Serve("localhost:3001")
	}()
}

func main() {
	startServer()

	for {
		time.Sleep(1 * time.Second)
	}
}
