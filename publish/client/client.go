package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"gitea.com/AcoSail/go-vsoa/client"
	"gitea.com/AcoSail/go-vsoa/protocol"
)

type PublishTestParam struct {
	Publish string `json:"publish"`
}

type callback struct{}

// VsoaGetPublishCall returns a description of the Go function.
//
// This function sets the client options, creates a new client instance,
// connects to the VSOA server, and closes the connection when the function
// exits. It then subscribes to the "/publisher" channel and retrieves the
// publish parameters. After a delay of 2 seconds, it unsubscribes from the
// "/publisher" channel and then subscribes again. It then waits for another
// 2 seconds before returning.
func VsoaGetPublishCall() {
	// Set client options
	clientOption := client.Option{
		Password: "123456",
	}

	// Create a new client instance
	c := client.NewClient(clientOption)

	// Connect to the VSOA server
	_, err := c.Connect("vsoa", "localhost:3001")
	if err != nil {
		fmt.Println(err)
		return
	}

	// Close the connection when the function exits
	defer c.Close()

	cb := new(callback)

	// Subscribe to the "/publisher" channel and retrieve the publish parameters
	err = c.Subscribe("/publisher", cb.getPublishParam)
	if err != nil {
		// Handle the error
		if err == errors.New(protocol.StatusText(protocol.StatusInvalidUrl)) {
			fmt.Println("Pass: Invalid URL")
			return
		} else {
			fmt.Println(err)
		}
	}

	// Wait for 2 seconds
	time.Sleep(2 * time.Second)

	// Unsubscribe from the "/publisher" channel
	c.UnSubscribe("/publisher")

	// Wait for 2 seconds
	time.Sleep(2 * time.Second)

	// Subscribe again to the "/publisher" channel
	c.Subscribe("/publisher", cb.getPublishParam)

	// Wait for 2 seconds
	time.Sleep(2 * time.Second)
}

func main() {
	VsoaGetPublishCall()
}

// getPublishParam parses the JSON parameter from the message and prints the "Publish" field
func (c callback) getPublishParam(m *protocol.Message) {
	// Create a new PublishTestParam instance
	param := new(PublishTestParam)

	// Unmarshal the JSON parameter into the param variable
	json.Unmarshal(m.Param, param)

	// Print the value of the "Publish" field
	fmt.Println("Param:", param.Publish)
}
