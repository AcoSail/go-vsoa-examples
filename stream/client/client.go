package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"

	"gitea.com/AcoSail/go-vsoa/client"
	"gitea.com/AcoSail/go-vsoa/protocol"
)

// VsoaStreamCall is a Go function that connects to the VSOA server,
// calls the "/read" RPC method, and receives data from the server.
//
// It sets the client options with a password, creates a new client
// instance, and connects to the VSOA server. It closes the connection
// when the function exits.
//
// The function then sends a request to the server using the "/read"
// method and prints the received data. If there is an error, it checks
// if the error is due to an invalid URL and prints a message accordingly.
//
// It also creates a new client stream, reads data from the stream, and
// writes the received data back to the stream server. Finally, it stops
// the client stream.
func VsoaStreamCall() {
	// Set client options
	clientOption := client.Option{
		Password: "123456",
	}

	var StreamTunID uint16
	streamDone := make(chan int)

	// Create a new client instance
	c := client.NewClient(clientOption)

	// Connect to the VSOA server
	_, err := c.Connect("vsoa", "localhost:3001")
	if err != nil {
		fmt.Println(err)
		return
	}

	// Close the connection when the function exits
	defer c.Close()

	req := protocol.NewMessage()

	// Send a request to the server using the "/read" method
	reply, err := c.Call("/read", protocol.TypeRPC, protocol.RpcMethodGet, req)
	if err != nil {
		// Check if the error is due to an invalid URL
		if err == errors.New(protocol.StatusText(protocol.StatusInvalidUrl)) {
			fmt.Println("Pass: Invalid URL")
		} else {
			fmt.Println(err)
		}
	} else {
		StreamTunID = reply.TunID()
		fmt.Println("Seq:", reply.SeqNo(), "Stream TunID:", StreamTunID)
	}

	receiveBuf := bytes.NewBufferString("")

	// Create a new client stream
	cs, err := c.NewClientStream(StreamTunID)
	if err != nil {
		fmt.Println(err)
	} else {
		go func() {
			buf := make([]byte, 32*1024)
			for {
				n, err := cs.Read(buf)
				if err != nil {
					// EOF means stream closed
					if err == io.EOF {
						break
					} else {
						fmt.Println(err)
						break
					}
				}
				receiveBuf.Write(buf[:n])
				fmt.Println("stream receiveBuf:", receiveBuf.String())

				// Add data to push back to server
				receiveBuf.WriteString(" received & push back to server")
				// Push data back to stream server
				cs.Write(receiveBuf.Bytes())

				// In this example, we just receive little data from server, so we just stop here
				goto STOP
			}

		STOP:
			cs.StopClientStream()
			streamDone <- 1
		}()
	}
	// Don't close the stream util the stream goroutine is done
	<-streamDone
}

func main() {
	VsoaStreamCall()
}
