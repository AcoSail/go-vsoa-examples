package main

import (
	"bytes"
	"fmt"
	"time"

	"gitea.com/AcoSail/go-vsoa/protocol"
	"gitea.com/AcoSail/go-vsoa/server"
)

// startServer initializes and starts the golang server.
func startServer() {
	// Initialize server options
	serverOption := server.Option{
		Password: "123456",
	}

	// Create a new server instance
	s := server.NewServer("golang VSOA stream server", serverOption)

	// Register URL and handler function
	h := func(req, res *protocol.Message) {
		// Create a new server stream
		ss, _ := s.NewSeverStream(res)

		// Prepare push and receive buffers
		pushBuf := bytes.NewBufferString("Golang VSOA stream server push Message")
		receiveBuf := bytes.NewBufferString("")

		// Start serving the server stream in a goroutine
		go func() {
			ss.ServeListener(pushBuf, receiveBuf)
			fmt.Println("stream server receiveBuf:", receiveBuf.String())
		}()
	}
	s.AddRpcHandler("/read", protocol.RpcMethodGet, h)

	// Start the server in a goroutine
	go func() {
		_ = s.Serve("127.0.0.1:3001")
	}()
}

func main() {
	startServer()

	for {
		time.Sleep(1 * time.Second)
	}
}
