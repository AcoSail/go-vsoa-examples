package main

import (
	"encoding/json"
	"net"
	"time"

	"gitea.com/AcoSail/go-vsoa/position"
	"gitea.com/AcoSail/go-vsoa/protocol"
	"gitea.com/AcoSail/go-vsoa/server"
)

type RpcLightParam struct {
	LightStatus bool `json:"Light On"`
}

var (
	lightstatus      = true
	vsoa_test_server = "vsoa_test_server"
)

// startServer initializes the Go VSOA server with a password and registers RPC handlers for the light URL.
//
// It sets the server password to "123456" and creates a new server instance with the given password.
// Then it registers two RPC handlers for the "/light" URL: one for the GET method and one for the SET method.
// The GET handler queries the current status of the light and returns it as a JSON string in the response parameter.
// The SET handler receives a request parameter containing the desired light status,
// updates the light status, and returns the updated status as a JSON string in the response parameter.
//
// There is also a goroutine that starts the server and listens for incoming requests on "localhost:3001".
func startServer() {
	// Initialize the Go VSOA server. In this example, the server's password is set to "123456".
	// If you don't need a password and have no other requirements, you can leave this part empty
	// and pass it as empty to the server.NewServer function.
	serverOption := server.Option{
		Password: "123456",
	}
	s := server.NewServer("golang VSOA RPC server", serverOption)

	// Register the light URL for the RPC GET method.
	// This allows authorized clients to query the current status of the light.
	handleLightGet := func(req, res *protocol.Message) {
		status, _ := json.Marshal(lightstatus)
		res.Param, _ = json.RawMessage(`{"Light On":` + string(status) + `}`).MarshalJSON()
		res.Data = req.Data
	}
	s.AddRpcHandler("/light", protocol.RpcMethodGet, handleLightGet)

	// Register the light URL for the RPC SET method.
	// This allows authorized clients to control the turning on or off of the light.
	handleLightSet := func(req, res *protocol.Message) {
		reqParam := new(RpcLightParam)
		err := json.Unmarshal(req.Param, reqParam)

		if err != nil {
			status, _ := json.Marshal(lightstatus)
			res.Param, _ = json.RawMessage(`{"Light On":` + string(status) + `}`).MarshalJSON()
			return
		}

		lightstatus = reqParam.LightStatus
		status, _ := json.Marshal(lightstatus)
		res.Param, _ = json.RawMessage(`{"Light On":` + string(status) + `}`).MarshalJSON()
		res.Data = req.Data
	}
	s.AddRpcHandler("/light", protocol.RpcMethodSet, handleLightSet)

	go func() {
		_ = s.Serve("localhost:3001")
	}()
}

// startPosition initializes a new position list and adds a new position to it.
// It then starts a position listener in a separate goroutine.
func startPosition() {
	// Create a new position list
	pl := position.NewPositionList()

	// Add a new position to the list
	pl.Add(*position.NewPosition(vsoa_test_server, 1, "127.0.0.1", 3001, false))

	// Start a position listener in a separate goroutine
	go pl.ServePositionListener(net.UDPAddr{
		IP:   net.ParseIP("127.0.0.1"),
		Port: 6001,
	})
}

func main() {
	startPosition()
	startServer()

	for {
		time.Sleep(1 * time.Second)
	}
}
